import React, { useState } from "react";
import "./App.css";
import Button from "./components/Button";
import Modal, { closeModal } from "./components/Modal";

function App() {
  const [openedModal, setOpenModal] = useState("None");

  const openModal = (modal) => {
    setOpenModal(modal);
  };

  return (
    <>
      <Button
        clickFunc={() => openModal("Delete")}
        text="Open first modal"
        backgroundColor="#FFA500"
      />
      <Button
        clickFunc={() => openModal("Confirm")}
        text="Open second modal"
        backgroundColor="#20B2AA"
      />

      {openedModal === "Delete" && (
        <Modal
          modalSetter={setOpenModal}
          header="Do you want to delete this file?"
          closeButton={true}
          text="Once you delete this file, it won’t be possible to undo this action. 
              Are you sure you want to delete it?"
          actions={
            <>
              <Button clickFunc={() => setOpenModal("None")} text="Ok" />
              <Button clickFunc={() => setOpenModal("None")} text="Cancel" />
            </>
          }
        />
      )}

      {openedModal === "Confirm" && (
        <Modal
          modalSetter={setOpenModal}
          header="Would you like to confirm this?"
          closeButton={false}
          text="Do you confirm this data? (This action cannot be undone in the future)"
          actions={
            <>
              <Button clickFunc={() => setOpenModal("None")} text="Yes" />
              <Button clickFunc={() => setOpenModal("None")} text="No" />
            </>
          }
        />
      )}
    </>
  );
}

export default App;
