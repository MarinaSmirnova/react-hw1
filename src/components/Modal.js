import React from "react";
import styled from "@emotion/styled";

const Overlay = styled.div`
  position: absolute;
  height: 100vh;
  width: 100vw;
  top: 0;
  left: 0;
  background-color: rgba(0, 0, 0, 0.5);
`;
const ModalDiv = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  min-height: 250px;
  width: 520px;
  background-color: #e74c3c;
  border-radius: 5px;
  transform: translate(-50%, -50%);
`;
const ModalHeader = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  border-radius: 5px 5px 0 0;
  background-color: #d44637;
  padding: 24px;
`;
const HeaderText = styled.h1`
  display: inline;
  color: #ffffff;
  font-size: 18px;
`;
const CloseBtn = styled.button`
  background: none;
  border: none;
  padding: 0;
  margin: 0 0 0 10px;
  color: #ffffff;
  font-size: 20px;
  height: 20px;
  width: 20px;
`;
const ModalText = styled.p`
  font-size: 16px;
  color: #ffffff;
  padding: 20px 40px;
  text-align: center;
  line-height: 2;
`;
const BtnContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 20px 15px;

  button {
    background-color: #b3382c;
    color: #ffffff;
    font-size: 16px;
    min-width: 100px;
    border: none;
    margin: 0 5px;
  }
`;

function Modal({ header, closeButton = true, text, actions, modalSetter }) {
  const closeModal = () => {
    modalSetter("None");
  };

  return (
    <Overlay
      onClick={(e) => {
        if (e.target === e.currentTarget) {
          closeModal();
        }
      }}
    >
      <ModalDiv>
        <ModalHeader>
          <HeaderText>{header}</HeaderText>
          {closeButton === true && <CloseBtn onClick={closeModal}>X</CloseBtn>}
        </ModalHeader>
        <ModalText>{text}</ModalText>
        <BtnContainer>{actions}</BtnContainer>
      </ModalDiv>
    </Overlay>
  );
}

export default Modal;
