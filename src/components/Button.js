import React from "react";
import styled from "@emotion/styled";

const ButtonTemplate = styled.button`
  padding: 15px;
  margin: 15px;
  font-size: 20px;
  border-radius: 7px;
`;

function Button({ backgroundColor, text, clickFunc }) {
  const styleProps = {
    backgroundColor: backgroundColor,
  };

  return (
    <ButtonTemplate onClick={clickFunc} style={styleProps}>
      {text}
    </ButtonTemplate>
  );
}

export default Button;
